package com.kfwebstandard.jspmodel1.persistence;

import java.io.IOException;

import com.kfwebstandard.jspmodel1.model.User;
import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserIO {

    private final static Logger LOG = LoggerFactory.getLogger(UserIO.class);

    /**
     * This method writes the data in the userBean to a file. In previous years
     * I taught that it was possible to write this file into the folder that
     * contained the web application. This is no longer the case as its a bad
     * practice. You must never write into the directory structure of a web app.
     * Instead you should write to an absolute location that can be configured
     * in the web.xml file.
     *
     * @param userBean
     * @param filename
     * @throws IOException
     */
    public synchronized static void addRecord(User userBean, String filename) throws IOException {
        LOG.debug("Filename = " + filename);
        Path emailList = Paths.get(filename);
        try (BufferedWriter writer = Files.newBufferedWriter(emailList, Charset.forName("UTF-8"), StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(userBean.getEmailAddress() + "|"
                    + userBean.getFirstName() + "|"
                    + userBean.getLastName() + "\n");
        }
    }

}
